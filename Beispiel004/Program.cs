﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beispiel004
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bitte geben sie eine Zahl ein.");
            string zahlText = Console.ReadLine();
            int zahl = Convert.ToInt32(zahlText);
            Console.WriteLine("Sie haben die Zahl " + zahl + " eingegeben.");
            Console.ReadKey();
        }
    }
}
